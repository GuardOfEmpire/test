#!/usr/bin/env php
<?php

include 'vendor/autoload.php';

use Symfony\Component\Console\Application;
use GuardOfEmpire\Dia2php\Generator\Parser;

$console = new Application('dia2php');

$console->addCommands([
    new Parser(),
]);

$console->run();