<?php

/**
 * Список классов диаграммы
 *
 * @author fess
 */
class DiaClassList
{
    protected $classes = [];
    
    public function createClass($name)
    {
        $class = new \DiaClass($name);
        
        $this->addClass($class);
        
        return $class;
    }
    
    public function addClass(\DiaClass $class)
    {
        $this->classes[] = $class;
        
        return $this;
    }
}
