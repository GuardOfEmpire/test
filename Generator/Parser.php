<?php

namespace GuardOfEmpire\Dia2php\Generator;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\LockHandler;

/**
 * Парсер
 * 
 * @author fess
 */
class Parser extends Command
{
    protected function configure()
    {
        $this
            ->setName('parse')
            ->setDescription('Парсер')
            ->addOption(
                'dia-file-name',
                'd',
                InputOption::VALUE_NONE,
                'Файл диаграммы'
            )
            ->addOption(
                'php-class-dir',
                null,
                InputOption::VALUE_NONE,
                'Каталог с исходниками'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('sss', OutputInterface::VERBOSITY_DEBUG);
    }
    
    /*
    public function __construct( $diaFileName )
    {
        $this->parse($diaFileName);
    }
    
    protected function parse($diaFileName)
    {
        $xml = $this->read($diaFileName);
    }
    
    protected function read($diaFileName)
    {
        if ( !is_readable($diaFileName) ) {
            throw new \Exception('Ошибка чтения файла');
        }
        
        $xml = new SimpleXMLElement(file_get_contents($diaFileName));
        
        return $xml;
    }
     */
}
