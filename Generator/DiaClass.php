<?php

/**
 * Класс из диаграммы
 */
class DiaClass
{
    protected $name = null;
    
    protected $extends = null;
    
    protected $attributeList = null;
    
    public function __construct($name)
    {
        $this->name = $name;
    }
}
